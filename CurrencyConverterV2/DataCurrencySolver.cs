﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyConverterV2
{
  public class DataCurrencySolver
  {
    Queue<DataConverter> fifo;
    DataCurrency dataCurrency;
    int result;

    public DataCurrencySolver()
    {
      result = -1;
      dataCurrency = new DataCurrency();
      fifo = new Queue<DataConverter>();
    }

    public void SetStartInformation(string start, string end, float money)
    {
      this.dataCurrency.SetStartInformation(start, end, money);

      DataConverter startInfo = new DataConverter(start, money);
      this.fifo.Enqueue(startInfo);
    }

    public void AddDataToGraph(string from, string to, float currency)
    {
      this.dataCurrency.AddDataToGraph(from, to, currency);
    }

    public void Bfs()
    {
      var seenCurrency = new HashSet<string>();

      while (this.fifo.Count > 0)
      {
        var elem = this.fifo.Dequeue();

        if (!seenCurrency.Contains(elem.from))
        {
          seenCurrency.Add(elem.from);
          var list = this.dataCurrency.GetValueByKey(elem.from);

          foreach (var neighborCurrency in list)
          {
            var from = neighborCurrency.to;
            float money = (float)Math.Round(neighborCurrency.currency * elem.money, 4);

            if (from == this.dataCurrency.GetStartInformation().to)
            {
              this.result = (int)Math.Round((double)money);
              return;
            }

            DataConverter tempData = new DataConverter(from, money);
            this.fifo.Enqueue(tempData);
          }
        }
      }
    }

    public int GetConvertion()
    {
      return this.result;
    }
  }
}
