﻿using System;

namespace CurrencyConverterV2
{
  class Program
  {
    static void Main(string[] args)
    {
      InputValidator inputValidator = new InputValidator();

      if (inputValidator.InputValidation(args))
      {
        try
        {
          DataCurrencySolver solver = new DataCurrencySolver();

          BuildData(solver, args[0]);
          solver.Bfs();

          Console.WriteLine(solver.GetConvertion());
        }
        catch (Exception e)
        {
          Console.WriteLine(e.ToString());
        }
      }
    }

    static void BuildData(DataCurrencySolver solver, string filePath)
    {
      ExtractData extractor = new ExtractData(filePath);

      GetStartInformation(solver, extractor);
      GetListCurrency(solver, extractor);
    }

    static void GetStartInformation(DataCurrencySolver solver, ExtractData extractor)
    {
      string start = null;
      string end = null;
      float money = 0F;

      extractor.GetStartInformation(ref start, ref end, ref money);
      solver.SetStartInformation(start, end, money);
    }

    // We get a line EUR;USD;1.2989 for exemple
    // We will save to times in graph:
    //  - First, EUR to USD with currency 1.2989
    //  - Second, USD to EUR with currency 1/1.2989 rounded at the 4th decimal
    static void GetListCurrency(DataCurrencySolver solver, ExtractData extractor)
    {
      var nbCurrency = 0;
      extractor.GetNumberCurrency(ref nbCurrency);

      for (; nbCurrency > 0; nbCurrency--)
      {
        string from = null;
        string to = null;
        float currency = 0F;

        extractor.GetNextCurrency(ref from, ref to, ref currency);
        solver.AddDataToGraph(from, to, currency);

        currency = (float)Math.Round(1 / currency, 4);
        solver.AddDataToGraph(to, from, currency);
      }
    }
  }
}
