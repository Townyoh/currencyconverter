﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyConverterV2
{
  public class InputValidator
  {
    public bool InputValidation(string[] args)
    {
      if (!this.NbArgument(args))
        return false;

      return this.ExtensionFile(args);
    }

    bool NbArgument(string[] args)
    {
      if (args.Length != 1)
        return false;

      return true;
    }

    bool ExtensionFile(string[] args)
    {
      return args[0].EndsWith(".txt");
    }
  }
}
