﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyConverterV2
{
  public struct DataConverter
  {
    public string from;
    public string to;
    public float currency;
    public float money;
    public bool isCurrency;

    public DataConverter(string f, string t, bool isC, float c)
    {
      from = f;
      to = t;
      currency = 0F;
      money = 0F;
      isCurrency = isC;

      if (isCurrency)
        currency = c;
      else
        money = c;
    }

    public DataConverter(string f, float m)
    {
      from = f;
      money = m;
      to = null;
      isCurrency = false;
      currency = 0F;
    }
  }

  public class DataCurrency
  {
    DataConverter startInformation;
    Dictionary<string, List<DataConverter>> graph;

    public DataCurrency()
    {
      graph = new Dictionary<string, List<DataConverter>>();
    }

    public void SetStartInformation(string start, string end, float money)
    {
      if (this.CheckInfo(start, end, money))
        this.startInformation = new DataConverter(start, end, false, money);
    }

    public void AddDataToGraph(string from, string to, float currency)
    {
      if (this.CheckInfo(from, to, currency))
      {
        DataConverter data = new DataConverter(from, to, true, currency);
        List<DataConverter> list = new List<DataConverter>();
        list.Add(data);

        if (!this.graph.TryAdd(from, list))
          this.graph[from].Add(data);
      }
    }

    public List<DataConverter> GetValueByKey(string key)
    {
      List<DataConverter> list = new List<DataConverter>();

      this.graph.TryGetValue(key, out list);

      return list;
    }

    public DataConverter GetStartInformation()
    {
      return this.startInformation;
    }

    bool CheckInfo(string first, string second, float data)
    {
      if (String.IsNullOrEmpty(first) || String.IsNullOrEmpty(second) || data.Equals(0F))
        return false;

      return true;
    }
  }
}
