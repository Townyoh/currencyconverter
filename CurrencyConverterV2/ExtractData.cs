﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;

namespace CurrencyConverterV2
{
  

  public class ExtractData
  {
    System.IO.StreamReader file;
    String filePath;

    public ExtractData(string path)
    {
      filePath = path;

      if (!File.Exists(filePath))
      {
        throw new Exception("File doesn't exist");
      }

      file = new StreamReader(filePath);
    }

    ~ExtractData()
    {
      file.Close();
    }

    // Exemple of input line => EUR;550;JPY
    public void GetStartInformation(ref string start, ref string end, ref float money)
    {
      string line = this.file.ReadLine();
      string[] splitLine = line.Split(';');
      if (splitLine.Length != 3)
        throw new Exception("Corrupted file.");

      start = splitLine[0];
      Single.TryParse(splitLine[1], NumberStyles.Float, CultureInfo.InvariantCulture, out money);
      //money = float.Parse(splitLine[1]);
      end = splitLine[2];
    }

    public void GetNumberCurrency(ref int nbCurrency)
    {
      nbCurrency = int.Parse(this.file.ReadLine());
    }

    // Exemple of input line => EUR;USD;1.2989
    public void GetNextCurrency(ref string from, ref string to, ref float currency)
    {
      string line = this.file.ReadLine();
      string[] splitLine = line.Split(';');
      if (splitLine.Length != 3)
        throw new Exception("Corrupted file.");

      from = splitLine[0];
      to = splitLine[1];
      Single.TryParse(splitLine[2], NumberStyles.Float, CultureInfo.InvariantCulture, out currency);
      //currency = float.Parse(splitLine[2].Trim());
    }
  }
}
