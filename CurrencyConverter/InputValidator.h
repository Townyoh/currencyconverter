#pragma once

class InputValidator
{
public:
  bool inputValidation(int argc, char** argv);

private:
  bool nbArgument(int argc);
  bool extensionFile(char** argv);
};
