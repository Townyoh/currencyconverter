#include <iso646.h>

#include "ExtractData.h"

ExtractData::ExtractData(const char* file)
  : filePath(file)
{
  stream.open(filePath);
  if (not stream.is_open())
  {
    throw "Unable to open file";
  }
}

ExtractData::~ExtractData()
{
  stream.close();
}

// Exemple of input line => EUR;550;JPY
void ExtractData::getStartInformation(std::string& start, std::string& end, float& money)
{
  auto temp = std::string();
  this->stream >> temp;

  start = temp.substr(0, temp.find_first_of(';'));
  end = temp.substr(temp.find_last_of(';') + 1);
  money = std::stof(temp.substr(temp.find_first_of(';') + 1, temp.find_last_of(';') - temp.find_first_of(';')));
}

void ExtractData::getNumberCurrency(int& nbCurrency)
{
  this->stream >> nbCurrency;
}

// Exemple of input line => EUR;USD;1.2989
void ExtractData::getNextCurrency(std::string& from, std::string& to, float& currency)
{
  auto temp = std::string();
  this->stream >> temp;

  this->convertLineToData(temp, from, to, currency);
}

void ExtractData::convertLineToData(const std::string& line, std::string& from, std::string& to, float& currency)
{
  from = std::string(line.substr(0, line.find_first_of(';')));
  to = std::string(line.substr(line.find_first_of(';') + 1, line.find_last_of(';') - line.find_first_of(';') - 1));
  currency = std::stof(line.substr(line.find_last_of(';') + 1));
}
