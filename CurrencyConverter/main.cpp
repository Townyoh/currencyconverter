#include <iostream>
#include <sstream>
#include <iomanip>

#include "InputValidator.h"
#include "ExtractData.h"
#include "DataCurrencySolver.h"

void getStartInfo(ExtractData& extractor, DataCurrencySolver& dataCurrencySolver)
{
  auto start = std::string();
  auto end = std::string();
  auto money = 0.f;

  extractor.getStartInformation(start, end, money);

  dataCurrencySolver.setStartInformation(start, end, money);
}

// We get a line EUR;USD;1.2989 for exemple
// We will save to times in graph:
//  - First, EUR to USD with currency 1.2989
//  - Second, USD to EUR with currency 1/1.2989 rounded at the 4th decimal
void getListCurrency(ExtractData& extractor, DataCurrencySolver& dataCurrencySolver)
{
  auto nbCurrency = 0;
  extractor.getNumberCurrency(nbCurrency);

  for (; nbCurrency > 0; nbCurrency--)
  {
    auto from = std::string();
    auto to = std::string();
    auto currency = 0.f;

    extractor.getNextCurrency(from, to, currency);
    dataCurrencySolver.addDataToGraph(from, to, currency);

    std::stringstream os;
    os << std::fixed << std::setprecision(4) << (float)(1 / currency);
    os >> currency;

    dataCurrencySolver.addDataToGraph(to, from, currency);
  }
}

void buildData(DataCurrencySolver& dataCurrencySolver, const char* file)
{
  ExtractData extractor(file);

  getStartInfo(extractor, dataCurrencySolver);
  getListCurrency(extractor, dataCurrencySolver);
}

int main(int argc, char** argv)
{
  InputValidator validator;

  if (validator.inputValidation(argc, argv))
  {
    try
    {
      DataCurrencySolver dataCurrencySolver;

      buildData(dataCurrencySolver, argv[1]);
      dataCurrencySolver.bfs();

      std::cout << dataCurrencySolver.getConvertion() << std::endl;
    }
    catch (const std::exception& e)
    {
      std::cerr << e.what() << std::endl;
    }
  }
}