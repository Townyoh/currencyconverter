#include <string>
#include <unordered_map>

struct DataConverter
{
  std::string from;
  std::string to;
  float currency;
  float money;
  bool isCurrency;

  DataConverter()
  {}

  DataConverter(std::string& f, std::string& t, bool is, float& c)
    : from(f), to(t), isCurrency(is), currency(0.f), money(0.f)
  {
    if (isCurrency)
      currency = c;
    else
      money = c;
  }

  DataConverter(std::string& f, float m)
    : from(f), money(m), to(""), isCurrency(false), currency(0.f)
  {}
};

class DataCurrency
{
  DataConverter startInformation;
  std::unordered_map<std::string, std::vector<DataConverter>> graph;

public:
  DataCurrency();
  void setStartInformation(std::string& start, std::string& end, float& money);
  void addDataToGraph(std::string& from, std::string& to, float& currency);
  std::vector<DataConverter> getValueForKey(const std::string& key);

  DataConverter getStartInformation()
  {
    return this->startInformation;
  }

private:
  bool checkInfo(const std::string& first, const std::string& second, const float data);
};
