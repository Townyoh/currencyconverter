#pragma once

#include <queue>

#include "DataCurrency.h"

class DataCurrencySolver
{
  std::queue<DataConverter> fifo;
  DataCurrency dataCurrency;
  int result;

public:
  DataCurrencySolver()
    : result(-1)
  {}

  void setStartInformation(std::string& start, std::string& end, float& money);
  void addDataToGraph(std::string& from, std::string& to, float& currency);
  void bfs();
  int getConvertion()
  {
    return this->result;
  }

private:
  bool browseNeighbor(const DataConverter& current);
};
