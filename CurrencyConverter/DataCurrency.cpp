#include <iso646.h>

#include "DataCurrency.h"

DataCurrency::DataCurrency()
{

}

void DataCurrency::setStartInformation(std::string& start, std::string& end, float& money)
{
  if (this->checkInfo(start, end, money))
    this->startInformation = DataConverter(start, end, false, money);
}

// Here DataConverter is use for saving currency between from and to
void DataCurrency::addDataToGraph(std::string& from, std::string& to, float& currency)
{
  if (this->checkInfo(from, to, currency))
  {
    DataConverter data(from, to, true, currency);

    auto tempList = std::vector<DataConverter>{ data };
    auto elem = this->graph.insert(std::make_pair(data.from, tempList));

    if (not elem.second)
    {
      elem.first->second.push_back(data);
    }
  }
}

std::vector<DataConverter> DataCurrency::getValueForKey(const std::string& key)
{
  auto elem = this->graph.find(key);

  if (elem == this->graph.end())
    return {};

  return elem->second;
}

bool DataCurrency::checkInfo(const std::string& first, const std::string& second, const float data)
{
  if (first.empty() || second.empty() || data == 0.f)
    return false;

  return true;
}
