#include <iostream>
#include <iso646.h>
#include <string>

#include "InputValidator.h"

// argc = 2 because the first argument is the executable himself so argv[0] = CurrencyConverter.exe
bool InputValidator::nbArgument(int argc)
{
  if (argc != 2)
  {
    std::cerr << "Argument number are wrong, " << argc - 1 << ". Should be 1." << std::endl;
    return false;
  }

  return true;
}

// Force extension to be .txt for now to avoid some file problem
bool InputValidator::extensionFile(char** argv)
{
  if (argv[1] == nullptr)
  {
    std::cerr << "File path isn't correct." << std::endl;
    return false;
  }

  auto pathFile = std::string(argv[1]);
  auto extension = pathFile.substr(pathFile.find_last_of('.') + 1);

  if (extension != "txt")
  {
    std::cerr << "Extension file isn't correct, " << extension << ". Should be 'txt'." << std::endl;
    return false;
  }

  return true;
}

bool InputValidator::inputValidation(int argc, char** argv)
{
  if (not nbArgument(argc))
    return false;

  if (not extensionFile(argv))
    return false;

  return true;
}
