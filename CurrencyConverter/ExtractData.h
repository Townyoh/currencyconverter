#pragma once

#include <string>
#include <fstream>

class ExtractData
{
  std::string filePath;
  std::ifstream stream;

public:
  ExtractData(const char* file);
  ~ExtractData();

  void getStartInformation(std::string& start, std::string& end, float& money);
  void getNumberCurrency(int& nbCurrency);
  void getNextCurrency(std::string& start, std::string& end, float& currency);

private:
  void convertLineToData(const std::string& line, std::string& from, std::string& to, float& currency);
};
