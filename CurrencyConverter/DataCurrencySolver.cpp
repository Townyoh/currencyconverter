#include <iso646.h>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <unordered_set>

#include "DataCurrencySolver.h"

void DataCurrencySolver::setStartInformation(std::string& start, std::string& end, float& money)
{
  this->dataCurrency.setStartInformation(start, end, money);

  DataConverter startInfo(start, money);
  this->fifo.push(startInfo);
}

void DataCurrencySolver::addDataToGraph(std::string& from, std::string& to, float& currency)
{
  this->dataCurrency.addDataToGraph(from, to, currency);
}

// To find the closest path between our start and end currency, we use a simple BFS (Breath First Search)
// A simple way because we don't need to save at which circle we are in our BFS
void DataCurrencySolver::bfs()
{
  auto seenCurrency = std::unordered_set<std::string>();

  while (not this->fifo.empty())
  {
    auto topElem = this->fifo.front();
    this->fifo.pop();

    auto inserted = seenCurrency.insert(topElem.from);
    if (not inserted.second)
      continue;

    if (this->browseNeighbor(topElem))
      return;
  }
}

// We save in our queue only from and the current convertion that we made with the previous currency
bool DataCurrencySolver::browseNeighbor(const DataConverter& current)
{
  auto listCurrency = this->dataCurrency.getValueForKey(current.from);

  for (const auto& neighborCurrency : listCurrency)
  {
    auto from = std::string(neighborCurrency.to);
    auto money = 0.f;

    std::stringstream os;
    os << std::fixed << std::setprecision(4) << current.money * neighborCurrency.currency;
    os >> money;

    if (from == this->dataCurrency.getStartInformation().to)
    {
      this->result = std::round(money);
      return true;
    }

    DataConverter newElem(from, money);
    this->fifo.push(newElem);
  }

  return false;
}