#include "pch.h"

#include "../CurrencyConverter/InputValidator.h"

TEST(InputValidator_UnitTest, ZeroArgument)
{
  InputValidator validator;
  auto argc = 1;
  char** argv = nullptr;

  EXPECT_FALSE(validator.inputValidation(argc, argv));
}

TEST(InputValidator_UnitTest, TooMuchArguments)
{
  InputValidator validator;
  auto argc = 3;
  char** argv = nullptr;

  EXPECT_FALSE(validator.inputValidation(argc, argv));
}

TEST(InputValidator_UnitTest, CorrectArguments)
{
  InputValidator validator;
  auto argc = 2;
  char **argv = new char*[2];
  for (int index = 0; index < 2; index++)
    argv[index] = new char[10];

  argv[1] = "file.txt";

  EXPECT_TRUE(validator.inputValidation(argc, argv));
}

