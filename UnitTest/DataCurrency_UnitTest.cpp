#include "pch.h"

#include "../CurrencyConverter/DataCurrency.h"

TEST(DataCurrency, CorrectStartInfo)
{
  auto start = std::string("EUR");
  auto end = std::string("JPY");
  auto money = 50.0f;

  DataCurrency dataCurrency;
  dataCurrency.setStartInformation(start, end, money);

  auto startInfo = dataCurrency.getStartInformation();
  EXPECT_EQ(startInfo.from, start);
  EXPECT_EQ(startInfo.to, end);
  EXPECT_EQ(startInfo.money, money);
  EXPECT_EQ(startInfo.currency, 0.f);
}

TEST(DataCurrency, CheckGraph)
{
  auto from = std::string("EUR");
  auto to = std::string("JPY");
  auto currency = 50.0f;

  DataCurrency dataCurrency;
  dataCurrency.addDataToGraph(from, to, currency);

  {
    auto list = dataCurrency.getValueForKey(from);
    EXPECT_EQ(list.size(), 1);

    EXPECT_EQ(list[0].from, from);
    EXPECT_EQ(list[0].to, to);
    EXPECT_EQ(list[0].currency, currency);
    EXPECT_EQ(list[0].money, 0.f);
  }

  {
    auto list = dataCurrency.getValueForKey(to);
    EXPECT_TRUE(list.empty());
  }
}