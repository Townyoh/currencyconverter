#include "pch.h"

#include "../CurrencyConverter/ExtractData.h"

TEST(ExtractData, FirstCurrencyTextFile)
{
  try
  {
    ExtractData extractor("..\\..\\TestFile\\FirstCurrency.txt");
    
    {
      auto start = std::string();
      auto end = std::string();
      auto money = 0.f;

      extractor.getStartInformation(start, end, money);

      EXPECT_EQ(start, "EUR");
      EXPECT_EQ(end, "JPY");
      EXPECT_EQ(money, 550.0);
    }

    {
      auto nbCurrency = 0;

      extractor.getNumberCurrency(nbCurrency);
      EXPECT_EQ(nbCurrency, 6);

      std::vector<std::tuple<std::string, std::string, float>> result
      {
        {"AUD", "CHF", 0.9661 },
        {"JPY", "KRW", 13.1151 },
        {"EUR", "CHF", 1.2053 },
        {"AUD", "JPY", 86.0305 },
        {"EUR", "USD", 1.2989 },
        {"JPY", "INR", 0.6571 }
      };

      for (auto index = 0; index < nbCurrency; index++)
      {
        auto from = std::string();
        auto to = std::string();
        auto currency = 0.f;

        extractor.getNextCurrency(from, to, currency);

        EXPECT_EQ(from, std::get<0>(result[index]));
        EXPECT_EQ(to, std::get<1>(result[index]));
        EXPECT_EQ(currency, std::get<2>(result[index]));
      }
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
}

