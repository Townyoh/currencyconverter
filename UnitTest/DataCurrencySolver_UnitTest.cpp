#include "pch.h"

#include "../CurrencyConverter/DataCurrencySolver.h"

TEST(DataCurrencySolver_UnitTest, CorrectDataForBFS)
{
  auto start = std::string("EUR");
  auto end = std::string("JPY");
  auto money = 550.f;
  DataCurrencySolver solver;
  solver.setStartInformation(start, end, money);

  std::vector<std::tuple<std::string, std::string, float>> data
  {
    {"AUD", "CHF", 0.9661 },
    {"JPY", "KRW", 13.1151 },
    {"EUR", "CHF", 1.2053 },
    {"AUD", "JPY", 86.0305 },
    {"EUR", "USD", 1.2989 },
    {"JPY", "INR", 0.6571 },
    {"CHF", "AUD", 1.0351 },
    {"KRW", "JPY", 0.0762 },
    {"CHF", "EUR", 0.8297 },
    {"JPY", "AUD", 0.0116 },
    {"USD", "EUR", 0.7699 },
    {"INR", "JPY", 1.5218 }
  };

  for (auto& elem : data)
  {
    solver.addDataToGraph(std::get<0>(elem), std::get<1>(elem), std::get<2>(elem));
  }

  solver.bfs();

  EXPECT_EQ(solver.getConvertion(), 59033);
}