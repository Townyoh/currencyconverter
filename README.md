# CurrencyConverter

A simple currency converter that read a file a start, end currency and a start money. With that a uncomplete list of currency.

Exemple :

EUR;550;JPY         // Start information (start;money;end)  
6                   // Number of currency available  
AUD;CHF;0.9661      // List of currency available (from;to;currency)  
JPY;KRW;13.1151  
EUR;CHF;1.2053  
AUD;JPY;86.0305  
EUR;USD;1.2989  
JPY;INR;0.6571  

