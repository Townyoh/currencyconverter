﻿using NUnit.Framework;

using CurrencyConverterV2;

namespace CurrencyConverter_UnitTest
{
  [TestFixture]
  class TestCurrencyConverterTest
  {
    [Test]
    public void CorrectStartInfo()
    {
      string start = new string("EUR");
      string end = new string("JPY");
      float money = 50F;

      DataCurrency dataCurrency = new DataCurrency();
      dataCurrency.SetStartInformation(start, end, money);
      var startInfo = dataCurrency.GetStartInformation();

      Assert.AreEqual(start, startInfo.from);
      Assert.AreEqual(end, startInfo.to);
      Assert.AreEqual(money, startInfo.money);
    }

    [Test]
    public void CheckInfoFromGraph()
    {
      string from = new string("EUR");
      string to = new string("JPY");
      float currency = 50F;

      DataCurrency dataCurrency = new DataCurrency();
      dataCurrency.AddDataToGraph(from, to, currency);

      {
        var list = dataCurrency.GetValueByKey(from);

        Assert.AreEqual(list.Count, 1);
        Assert.AreEqual(list[0].from, from);
        Assert.AreEqual(list[0].to, to);
        Assert.AreEqual(list[0].currency, currency);
        Assert.AreEqual(list[0].isCurrency, true);
      }

      {
        var list = dataCurrency.GetValueByKey(to);

        Assert.AreEqual(list, null);
      }
    }

  }
}
