﻿using NUnit.Framework;
using System;

using CurrencyConverterV2;
using System.Collections.Generic;

namespace ExtractData_UnitTest
{
  [TestFixture]
  class TestExtractData
  {
    [Test]
    public void FirstCurrencyTextFile()
    {
      ExtractData extractData = new ExtractData("..\\..\\..\\..\\TestFile\\FirstCurrency.txt");

      {
        string start = null;
        string end = null;
        float money = 0;

        extractData.GetStartInformation(ref start, ref end, ref money);

        Assert.AreEqual(start, "EUR");
        Assert.AreEqual(end, "JPY");
        Assert.AreEqual(money, 550.0);
      }

      {
        int nbCurrency = 0;
        extractData.GetNumberCurrency(ref nbCurrency);
        Assert.AreEqual(nbCurrency, 6);

        var result = new (string, string, float)[]
        {
          ("AUD", "CHF", 0.9661F ),
          ("JPY", "KRW", 13.1151F ),
          ("EUR", "CHF", 1.2053F ),
          ("AUD", "JPY", 86.0305F ),
          ("EUR", "USD", 1.2989F ),
          ("JPY", "INR", 0.6571F )
        };

        for (int index = 0; index < nbCurrency; index++)
        {
          string from = null;
          string to = null;
          float currency = 0F;

          extractData.GetNextCurrency(ref from, ref to, ref currency);

          Assert.AreEqual(from, result[index].Item1);
          Assert.AreEqual(to, result[index].Item2);
          Assert.AreEqual(currency, result[index].Item3);
        }
      }
    }

  }
}
