using NUnit.Framework;

using CurrencyConverterV2;

namespace InputValidator_UnitTest
{
  [TestFixture]
  public class TestInputValidator
  {
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void CorrectArgument()
    {
      string[] args = new string[1] { "MyFile.txt" };

      InputValidator validator = new InputValidator();

      Assert.True(validator.InputValidation(args));
    }

    [Test]
    public void WrongArgumentNumber()
    {
      string[] args = new string[2] { "MyFile.txt", "OtherFile.txt" };

      InputValidator validator = new InputValidator();

      Assert.False(validator.InputValidation(args));
    }

    [Test]
    public void WrongExtensionFile()
    {
      string[] args = new string[1] { "MyFile.jpeg" };

      InputValidator validator = new InputValidator();

      Assert.False(validator.InputValidation(args));
    }
  }
}