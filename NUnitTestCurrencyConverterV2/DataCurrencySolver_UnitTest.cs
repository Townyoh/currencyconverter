﻿using NUnit.Framework;

using CurrencyConverterV2;

namespace DataCurrencySolver_UnitTest
{
  [TestFixture]
  class TestDataCurrencySolver
  {
    [Test]
    public void CorrectDataForBFS()
    {
      var start = new string("EUR");
      var end = new string("JPY");
      var money = 550F;

      DataCurrencySolver solver = new DataCurrencySolver();
      solver.SetStartInformation(start, end, money);

      var result = new (string, string, float)[]
      {
        ("AUD", "CHF", 0.9661F ),
        ("JPY", "KRW", 13.1151F ),
        ("EUR", "CHF", 1.2053F ),
        ("AUD", "JPY", 86.0305F ),
        ("EUR", "USD", 1.2989F ),
        ("JPY", "INR", 0.6571F ),
        ("CHF", "AUD", 1.0351F ),
        ("KRW", "JPY", 0.0762F ),
        ("CHF", "EUR", 0.8297F ),
        ("JPY", "AUD", 0.0116F ),
        ("USD", "EUR", 0.7699F ),
        ("INR", "JPY", 1.5218F )
      };

      foreach (var elem in result)
        solver.AddDataToGraph(elem.Item1, elem.Item2, elem.Item3);

      solver.Bfs();

      Assert.AreEqual(solver.GetConvertion(), 59033);
    }
  } 
}
